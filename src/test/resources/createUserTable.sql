-- Best practice MySQL as of 5.7.6
--
USE jsf_password;

DROP TABLE IF EXISTS user_table;
CREATE TABLE user_table (
  login_name VARCHAR(45) NOT NULL,
  password VARCHAR(12) NOT NULL,
  PRIMARY KEY (login_name)
) ENGINE=InnoDB;

