--
-- This script needs to run only once
--
DROP DATABASE IF EXISTS jsf_password;
CREATE DATABASE jsf_password;

-- Best practice MySQL as of 5.7.6
DROP USER IF EXISTS user@localhost;
CREATE USER user@'localhost' IDENTIFIED WITH mysql_native_password BY 'pencil' REQUIRE NONE;
GRANT ALL ON jsf_password.* TO user@'localhost';

-- Best practice MySQL prior to 5.7.6
-- GRANT ALL PRIVILEGES ON jsf_password.* TO user@'localhost' IDENTIFIED BY 'pencil';

FLUSH PRIVILEGES;