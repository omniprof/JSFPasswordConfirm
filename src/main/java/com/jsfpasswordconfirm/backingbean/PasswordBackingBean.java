package com.jsfpasswordconfirm.backingbean;

import com.jsfpasswordconfirm.controller.UserTableJpaController;
import com.jsfpasswordconfirm.entities.User;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for index.xhtml
 *
 * @author Ken Fogel
 */
@Named("passwordBacking")
@RequestScoped
public class PasswordBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PasswordBackingBean.class);

    @Inject
    private UserTableJpaController controller;

    // The entity object
    private User user;
    // The value for the confirmPassword field that does not exist in the entity
    private String passwordConfirm;

    /**
     * User created if it does not exist.
     *
     * @return
     */
    public User getUser() {
        LOG.trace("getUser");
        if (user == null) {
            LOG.debug("Creating new User entity object");
            user = new User();
        }
        return user;
    }

    /**
     * Save the current person to the db
     *
     * @return
     * @throws Exception
     */
    public String createUser() throws Exception {
        LOG.trace("Creating new User entity object");
        controller.create(user);
        return null;
    }

    /**
     * Getter for the password confirm field
     *
     * @return
     */
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    /**
     * Setter for the password confirm field
     *
     * @param passwordConfirm
     */
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    /**
     * The method that compares the two password fields incorrectly as it uses
     * the bean values. These values are not set until validation is complete so
     * a NullPointerException is thrown.
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordError01(FacesContext context, UIComponent component,
            Object value) {

        LOG.trace("validatePasswordError01");
        LOG.debug("Comparing passwords " + user.getPassword() + " and " + passwordConfirm);
        if (!user.getPassword().equals(passwordConfirm)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }

    /**
     * The method that compares the two password fields correctly by retrieving
     * the UIInput component for the first password input, where the parameter
     * is kept until they are applied to the bean. The problem with this method
     * is that it does not take into account the possibility that either that
     * password_confirm have not been set and if so then a NullPointerException
     * is thrown.
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordError02(FacesContext context, UIComponent component,
            Object value) {

        // Retrieve the value passed to this method
        String confirmPassword = (String) value;
        LOG.debug("confirmPassword: " + confirmPassword);

        // Retrieve the temporary value from the password field
        UIInput passwordInput = (UIInput) component.findComponent("password");

        String password = (String) passwordInput.getLocalValue();
        LOG.debug("password: " + password);

        LOG.debug("Comparing passwords " + password + " and " + confirmPassword);
        if (!password.equals(confirmPassword)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            passwordInput.resetValue();
            throw new ValidatorException(msg);
        }
    }

    /**
     * The method that compares the two password fields correctly by retrieving
     * the UIInput component for the first password input, where the parameter
     * is kept until they are applied to the bean. In this version we include
     * checks for null. These checks must come first in the if statement before
     * we compare the passwords. However, if we are using AJAX we might not get
     * the results we want. The student had made only the password_confirm field
     * AJAX enabled. This meant that when AJAX resulted in the validator running
     * the password field was ignored. Therefore it will always be null. The
     * solution is to make both fields AJAX aware.
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordCorrect01(FacesContext context, UIComponent component,
            Object value) {

        // Retrieve the value passed to this method
        String confirmPassword = (String) value;
        LOG.debug("passwordConfirm: " + passwordConfirm);

        // Retrieve the temporary value from the password field
        UIInput passwordInput = (UIInput) component.findComponent("password");

        String password = (String) passwordInput.getLocalValue();
        LOG.debug("password: " + password);

        LOG.debug("Comparing passwords " + password + " and " + confirmPassword);

        if (password == null || confirmPassword == null || !password.equals(confirmPassword)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            passwordInput.resetValue();
            throw new ValidatorException(msg);
        }

    }
}
